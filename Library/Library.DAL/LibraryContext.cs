﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Library.Domain;

namespace Library.DAL
{
    public class LibraryContext : DbContext 
    {
        public LibraryContext() : base("LibraryContext") { }
        public DbSet<Book> Books { set; get; }
        public DbSet<Writer> Writers { set; get; }
        public DbSet<Article> Articles { set; get; }
        public DbSet<WriterInBook> WriterInBooks { set; get; }
        public DbSet<WriterInArticle> WriterInArticles { set; get; }
    }
}