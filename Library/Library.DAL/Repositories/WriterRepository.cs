﻿using Library.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class WriterRepository
    {
        private LibraryContext _context;
        public WriterRepository(LibraryContext _context)
        {
            this._context = _context;
        }
        public IEnumerable<Writer> GetAll()
        {
            return _context.Writers;
        }
 
        public Writer Get(int id)
        {
            return _context.Writers.Find(id);
        }
        public Writer Last()
        {
            return _context.Writers.ToList().Last();
        }
        public int Count()
        {
            return _context.Writers.Count();
        }
        public void Create(Writer writer)
        {
            _context.Writers.Add(writer);
        }
 
        public void Update(Writer writer)
        {
            _context.Writers.Attach(writer);
            _context.Entry(writer).State = EntityState.Modified;
        }
 
        public IEnumerable<Writer> Find(Func<Writer, Boolean> predicate)
        {
            return _context.Writers.Where(predicate).ToList();
        }
 
        public void Delete(int id)
        {
            Writer book = _context.Writers.Find(id);
            if (book != null)
                _context.Writers.Remove(book);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
