﻿using Library.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class BookRepository
    {
        private LibraryContext _context;
        public BookRepository(LibraryContext _context)
        {
            this._context = _context;
        }
        public IEnumerable<Book> GetAll()
        {
            return _context.Books;
        }
 
        public Book Get(int id)
        {
            return _context.Books.Find(id);
        }
        public Book Last()
        {
            return _context.Books.ToList().Last();
        }
        public int Count()
        {
            return _context.Books.ToList().Count();
        }
 
        public void Create(Book book)
        {
            _context.Books.Add(book);
        }

        public void Update(Book book)
        {
            _context.Books.Attach(book);
            _context.Entry(book).State = EntityState.Modified;
        }
 
        public IEnumerable<Book> Find(Func<Book, Boolean> predicate)
        {
            return _context.Books.Where(predicate).ToList();
        }
 
        public void Delete(int id)
        {
            Book book = _context.Books.Find(id);
            if (book != null)
                _context.Books.Remove(book);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
