﻿using Library.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class WriterInBookRepository
    {
        private LibraryContext _context;
        public WriterInBookRepository(LibraryContext _context)
        {
            this._context = _context;
        }
        public IEnumerable<WriterInBook> GetAll()
        {
            return _context.WriterInBooks;
        }

        public WriterInBook Get(int id)
        {
            return _context.WriterInBooks.Find(id);
        }
        public WriterInBook Last()
        {
            return _context.WriterInBooks.Last();
        }
        public IEnumerable<Book> GetBooksByWriterId(int writerId)
        {
            return _context.WriterInBooks.Where(writerInBook => writerInBook.Writer.Id == writerId).Select(writerInBook => writerInBook.Book);
        }
        public IEnumerable<Writer> GetWritersByBookId(int bookId)
        {
            return _context.WriterInBooks.Where(writerInBook => writerInBook.Book.Id == bookId).Select(writerInBook => writerInBook.Writer);
        }
        public int Count()
        {
            return _context.WriterInBooks.Count();
        }
        public void Create(WriterInBook writerInBook)
        {
            _context.WriterInBooks.Add(writerInBook);
        }
        public void Create(Writer writer, Book book)
        {
            var tempWriterInBook = new WriterInBook();
            tempWriterInBook.Book = book;
            tempWriterInBook.Writer = writer;
            _context.WriterInBooks.Add(tempWriterInBook);
        }

        public void Update(WriterInBook writerInBook)
        {
            _context.WriterInBooks.Attach(writerInBook);
            _context.Entry(writerInBook).State = EntityState.Modified;
        }

        public IEnumerable<WriterInBook> Find(Func<WriterInBook, Boolean> predicate)
        {
            return _context.WriterInBooks.Where(predicate);
        }

        public void Delete(int id)
        {
            WriterInBook WriterInBook = _context.WriterInBooks.Find(id);
            if (WriterInBook != null)
                _context.WriterInBooks.Remove(WriterInBook);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
