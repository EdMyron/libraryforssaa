﻿using Library.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class ArticleRepository
    {
        private LibraryContext _context;
        public ArticleRepository(LibraryContext _context)
        {
            this._context = _context;
        }
        public IEnumerable<Article> GetAll()
        {
            return _context.Articles;
        }

        public Article Get(int id)
        {
            return _context.Articles.Find(id);
        }
        public Article Last()
        {
            return _context.Articles.ToList().Last();
        }
        public int Count()
        {
            return _context.Articles.ToList().Count();
        }

        public void Create(Article article)
        {
            _context.Articles.Add(article);
        }

        public void Update(Article article)
        {
            _context.Articles.Attach(article);
            _context.Entry(article).State = EntityState.Modified;
        }

        public IEnumerable<Article> Find(Func<Article, Boolean> predicate)
        {
            return _context.Articles.Where(predicate).ToList();
        }
        public void Delete(int id)
        {
            Article article = _context.Articles.Find(id);
            if (article != null)
                _context.Articles.Remove(article);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
