﻿using Library.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.DAL.Repositories
{
    public class WriterInArticleRepository
    {
        private LibraryContext _context;
        public WriterInArticleRepository(LibraryContext _context)
        {
            this._context = _context;
        }
        public IEnumerable<WriterInArticle> GetAll()
        {
            return _context.WriterInArticles;
        }
        public WriterInArticle Get(int id)
        {
            return _context.WriterInArticles.Find(id);
        }
        public WriterInArticle Last()
        {
            return _context.WriterInArticles.Last();
        }
        public int Count()
        {
            return _context.WriterInArticles.Count();
        }
        public void Create(WriterInArticle writerInArticle)
        {
            _context.WriterInArticles.Add(writerInArticle);
        }
        public void Create(Writer writer, Article article)
        {
            var tempWriterInArticle = new WriterInArticle();
            tempWriterInArticle.Article = article;
            tempWriterInArticle.Writer = writer;
            _context.WriterInArticles.Add(tempWriterInArticle);
        }

        public void Update(WriterInArticle writerInArticle)
        {
            _context.WriterInArticles.Attach(writerInArticle);
            _context.Entry(writerInArticle).State = EntityState.Modified;
        }
        public IEnumerable<Article> GetArticlesByWriterId(int writerId)
        {
            return _context.WriterInArticles.Where(writerInArticle => writerInArticle.Writer.Id == writerId).Select(writerInArticle => writerInArticle.Article);
        }
        public IEnumerable<Writer> GetWritersByArticleId(int bookId)
        {
            return _context.WriterInArticles.Where(writerInArticle => writerInArticle.Article.Id == bookId).Select(writerInArticle => writerInArticle.Writer);
        }

        public IEnumerable<WriterInArticle> Find(Func<WriterInArticle, Boolean> predicate)
        {
            return _context.WriterInArticles.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            WriterInArticle WriterInArticle = _context.WriterInArticles.Find(id);
            if (WriterInArticle != null)
                _context.WriterInArticles.Remove(WriterInArticle);
        }
        public void Save()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
