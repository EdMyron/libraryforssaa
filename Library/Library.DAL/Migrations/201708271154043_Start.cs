namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Writers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WriterBooks",
                c => new
                    {
                        BookID = c.Int(nullable: false),
                        WriterID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BookID, t.WriterID })
                .ForeignKey("dbo.Books", t => t.BookID, cascadeDelete: true)
                .ForeignKey("dbo.Writers", t => t.WriterID, cascadeDelete: true)
                .Index(t => t.BookID)
                .Index(t => t.WriterID);
            
            CreateTable(
                "dbo.WriterArticles",
                c => new
                    {
                        ArticleID = c.Int(nullable: false),
                        WriterID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ArticleID, t.WriterID })
                .ForeignKey("dbo.Articles", t => t.ArticleID, cascadeDelete: true)
                .ForeignKey("dbo.Writers", t => t.WriterID, cascadeDelete: true)
                .Index(t => t.ArticleID)
                .Index(t => t.WriterID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WriterArticles", "WriterID", "dbo.Writers");
            DropForeignKey("dbo.WriterArticles", "ArticleID", "dbo.Articles");
            DropForeignKey("dbo.WriterBooks", "WriterID", "dbo.Writers");
            DropForeignKey("dbo.WriterBooks", "BookID", "dbo.Books");
            DropIndex("dbo.WriterArticles", new[] { "WriterID" });
            DropIndex("dbo.WriterArticles", new[] { "ArticleID" });
            DropIndex("dbo.WriterBooks", new[] { "WriterID" });
            DropIndex("dbo.WriterBooks", new[] { "BookID" });
            DropTable("dbo.WriterArticles");
            DropTable("dbo.WriterBooks");
            DropTable("dbo.Books");
            DropTable("dbo.Writers");
            DropTable("dbo.Articles");
        }
    }
}
