namespace DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Library.Domain;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<Library.DAL.LibraryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Library.DAL.LibraryContext context)
        {
            if (context.Writers.Count() == 0)
            {
                CreateWriters(5, context);
            }
            if (context.Articles.Count() == 0)
            {
                CreateArticles(5, context);
            }
            if (context.Books.Count() == 0)
            {
                CreateBooks(5, context);
            }
        }
        private void CreateWriters(int count, Library.DAL.LibraryContext context)
        {
             var tempWriters = new List<Writer>();
                for (int i = 0; i < count; i++)
                {
                    tempWriters.Add(new Writer
                    {
                        Name = "TestWriter" + i+1,
                        BirthDate = new DateTime(2000+i+1,i+1,i+1)
                    });
                }
                context.Writers.AddRange(tempWriters);
                context.SaveChanges();
        }
        private void CreateArticles(int count, Library.DAL.LibraryContext context)
        {
            var tempArticles = new List<Article>();
            var tempWriterInArticles = new List<WriterInArticle>();
            for (int i = 0; i < count; i++)
            {
                tempArticles.Add(new Article
                {
                    Name = "TestArticle" + i+1,
                    PublishDate = new DateTime(2000 + i+1, i+1, i+1)
                });
                foreach (var writer in context.Writers)
                {
                    tempWriterInArticles.Add(new WriterInArticle
                    {
                        Article = tempArticles.Last(),
                        Writer = writer
                    });
                    context.WriterInArticles.AddRange(tempWriterInArticles);
                }
            }
            context.Articles.AddRange(tempArticles);
            context.SaveChanges();
        }
        private void CreateBooks(int count, Library.DAL.LibraryContext context)
        {
            var tempBooks = new List<Book>();
            var tempWriterInBooks = new List<WriterInBook>();
            for (int i = 0; i < count; i++)
            {
                tempBooks.Add(new Book
                {
                    Name = "TestBook" + i + 1,
                    PublishDate = new DateTime(2000 + i + 1, i + 1, i + 1)
                });
                foreach (var writer in context.Writers)
                {
                    tempWriterInBooks.Add(new WriterInBook
                    {
                        Book = tempBooks.Last(),
                        Writer = writer
                    });
                    context.WriterInBooks.AddRange(tempWriterInBooks);
                }
            }
            context.Books.AddRange(tempBooks);
            context.SaveChanges();
        }
    }
}
