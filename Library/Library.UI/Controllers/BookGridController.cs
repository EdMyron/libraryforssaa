﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Library.ViewModels.BookGirdViewModels;

namespace Library.UI.Controllers
{
    public class BookGridController : Controller
    {
        Library.BLL.BookGridService bookGridService = new Library.BLL.BookGridService();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ReadWriters([DataSourceRequest]DataSourceRequest request, int bookID)
        {
            return Json(bookGridService.GetEntityWritersList(bookID).ToDataSourceResult(request));
        }
        public ActionResult CreateWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writerViewModel, int bookID)
        {
            if (ModelState.IsValid)
            {
                bookGridService.CreateWriter(writerViewModel, bookID);
            }
            return Json(new[] { writerViewModel }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult UpdateWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writer)
        {
            if (ModelState.IsValid)
            {
                bookGridService.UpdateWriter(writer);
            }
            return Json(new[] { writer }.ToDataSourceResult(request, ModelState));
        }
        public ActionResult DestroyWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writer)
        {
            if (ModelState.IsValid)
            {
                bookGridService.DestroyWriter(writer);
            }
            return Json(new[] { writer }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult ReadBooks([DataSourceRequest]DataSourceRequest request)
        {
            return Json(bookGridService.getBooksList().ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult CreateBook([DataSourceRequest]DataSourceRequest request, BooksViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                bookGridService.CreateBook(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult UpdateBook([DataSourceRequest]DataSourceRequest request, BooksViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                bookGridService.UpdateBook(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult DestroyBook([DataSourceRequest]DataSourceRequest request, BooksViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                bookGridService.DestroyBook(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        protected override void Dispose(bool disposing)
        {
            bookGridService.Dispose();
            base.Dispose(disposing);
        }
    }
}