﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Library.ViewModels.ArticleGirdViewModels;

namespace Library.UI.Controllers
{
    public class ArticleGridController : Controller
    {
        Library.BLL.ArticleGridService articleGridService = new Library.BLL.ArticleGridService();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ReadWriters([DataSourceRequest]DataSourceRequest request, int articleId)
        {
            return Json(articleGridService.GetEntityWritersList(articleId).ToDataSourceResult(request));
        }
        [HttpPost]
        public ActionResult CreateWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writerViewModel, int articleId)
        {
            if (ModelState.IsValid)
            {
                articleGridService.CreateWriter(writerViewModel, articleId);
            }
            return Json(new[] { writerViewModel }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult UpdateWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writer)
        {
            if (ModelState.IsValid)
            {
                articleGridService.UpdateWriter(writer);
            }
            return Json(new[] { writer }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult DestroyWriter([DataSourceRequest]DataSourceRequest request, WriterViewModel writer)
        {
            if (ModelState.IsValid)
            {
                articleGridService.DestroyWriter(writer);
            }
            return Json(new[] { writer }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult ReadArticles([DataSourceRequest]DataSourceRequest request)
        {
            return Json(articleGridService.getArticlesList().ToDataSourceResult(request));
        }

        [HttpPost]
        public ActionResult CreateArticle([DataSourceRequest]DataSourceRequest request, ArticleViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                articleGridService.CreateArticle(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult UpdateArticle([DataSourceRequest]DataSourceRequest request, ArticleViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                articleGridService.UpdateArticle(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        [HttpPost]
        public ActionResult DestroyArticle([DataSourceRequest]DataSourceRequest request, ArticleViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                articleGridService.DestroyArticle(viewModel);
            }
            return Json(new[] { viewModel }.ToDataSourceResult(request, ModelState));
        }
        protected override void Dispose(bool disposing)
        {
            articleGridService.Dispose();
            base.Dispose(disposing);
        }
    }
}