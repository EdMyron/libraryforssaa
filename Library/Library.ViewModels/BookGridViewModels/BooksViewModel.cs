﻿using System;
using Library.Domain;
using System.ComponentModel.DataAnnotations;

namespace Library.ViewModels.BookGirdViewModels
{
    public class BooksViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime PublishDate { set; get; }
    }
}
