﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ViewModels.ArticleGirdViewModels
{
    public class ArticleViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime PublishDate { set; get; }
    }
}
