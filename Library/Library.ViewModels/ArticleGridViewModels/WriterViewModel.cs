﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.Domain;

namespace Library.ViewModels.ArticleGirdViewModels
{
    public class WriterViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime BirthDate { set; get; }
    }
}