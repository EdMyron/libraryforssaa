﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Domain
{
    public class Book
    {
        public Book()
        {
            this.WriterInBooks = new List<WriterInBook>();
        }
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime PublishDate { set; get; }
        public virtual List<WriterInBook> WriterInBooks { set; get; }
    }
}