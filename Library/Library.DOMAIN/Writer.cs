﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Domain
{
    public class Writer
    {
        public Writer()
        {
            this.WriterInBooks = new List<WriterInBook>();
            this.WriterInArticles = new List<WriterInArticle>();
        }
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime BirthDate { set; get; }
        public virtual List<WriterInBook> WriterInBooks{set;get;}
        public virtual List<WriterInArticle> WriterInArticles { set; get; }
    }
}