﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Domain
{
    public class Article
    {
        public Article()
        {
            this.WriterInArticles = new List<WriterInArticle>(); 
        }
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime PublishDate { set; get; } 
        public virtual List<WriterInArticle> WriterInArticles { set; get; }
    }
}