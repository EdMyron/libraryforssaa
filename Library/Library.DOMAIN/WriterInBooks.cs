﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Domain
{
    public class WriterInBook
    {
        public int Id { set; get; }
        [Required]
        public virtual Writer Writer { set; get; }
        [Required]
        public virtual Book Book { set; get; }
    }
}
