﻿using Library.DAL;
using Library.DAL.Repositories;
using Library.Domain;
using Library.ViewModels.BookGirdViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL
{
    public class BookGridService
    {
        private WriterInBookRepository _writerInBookRepository;
        private WriterRepository _writerRepository;
        private BookRepository _bookRepository;
        public BookGridService()
        {
            var context = new LibraryContext();
            _writerInBookRepository = new WriterInBookRepository(context);
            _writerRepository = new WriterRepository(context);
            _bookRepository = new BookRepository(context);
        }
        public List<WriterViewModel> GetEntityWritersList(int bookId)
        {
            var result = new List<WriterViewModel>();
            var writers = _writerInBookRepository.GetWritersByBookId(bookId);
            foreach (var writer in writers)
            { 
                result.Add(new WriterViewModel
                {
                    Id = writer.Id ,
                    Name = writer.Name,
                    BirthDate = writer.BirthDate
                });
            }

            return result;
        }
        public void CreateWriter(WriterViewModel writer, int entityId)
        {
            var entity = new Writer
            {
                Name = writer.Name,
                BirthDate = writer.BirthDate
            };
            var tempBook = _bookRepository.Find(book => book.Id == entityId).First();
            var tempWriterInBook = new WriterInBook();
            tempWriterInBook.Writer = entity;
            tempWriterInBook.Book = tempBook;
            tempBook.WriterInBooks.Add(tempWriterInBook);
            entity.WriterInBooks.Add(tempWriterInBook);
            _writerRepository.Create(entity);
            _writerInBookRepository.Create(tempWriterInBook);
            _bookRepository.Update(tempBook);
            _writerInBookRepository.Save();
            writer.Id = _writerRepository.Last().Id;
        }
        public void UpdateWriter(WriterViewModel writer)
        {
            var newWriter = new Writer
            {
                Id = writer.Id,
                Name = writer.Name,
                BirthDate = writer.BirthDate,
            };
            _writerRepository.Update(newWriter);
            _writerRepository.Save();
        }
        public void DestroyWriter(WriterViewModel writer)
        {
            _writerRepository.Delete(writer.Id);
            _writerRepository.Save();
        }
        public List<BooksViewModel> getBooksList()
        {
            var result = new List<BooksViewModel>();
            var books = _bookRepository.GetAll();
            foreach (var book in books)
            {
                result.Add(new BooksViewModel
                {
                    Id = book.Id,
                    Name = book.Name,
                    PublishDate = book.PublishDate
                });
            }
            return result;
        }
        public void CreateBook(BooksViewModel viewModel)
        {
            var entity = new Book
            {
                Name = viewModel.Name,
                PublishDate = viewModel.PublishDate
            };
            _bookRepository.Create(entity);
            _bookRepository.Save();
            viewModel.Id = _bookRepository.Last().Id;
        }

        public void UpdateBook(BooksViewModel viewModel)
        {
            var entity = new Book
            {
                Id=viewModel.Id,
                Name = viewModel.Name,
                PublishDate = viewModel.PublishDate
            };
            _bookRepository.Update(entity);
            _bookRepository.Save();
        }

        public void DestroyBook(BooksViewModel viewModel)
        {
            _bookRepository.Delete(viewModel.Id);
            _bookRepository.Save();
        }
        public void Dispose()
        {
            _bookRepository.Dispose();
            _writerInBookRepository.Dispose();
            _writerRepository.Dispose();
        }
    }
}
