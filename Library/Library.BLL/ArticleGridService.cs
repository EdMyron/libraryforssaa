﻿using Library.DAL;
using Library.DAL.Repositories;
using Library.Domain;
using Library.ViewModels.ArticleGirdViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.BLL
{
    public class ArticleGridService
    {
        private WriterInArticleRepository _writerInArticelRepository;
        private WriterRepository _writerRepository;
        private ArticleRepository _articleRepository;
        public ArticleGridService()
        {
            var context = new LibraryContext();
            _writerInArticelRepository = new WriterInArticleRepository(context);
            _writerRepository = new WriterRepository(context);
            _articleRepository = new ArticleRepository(context);
        }
        public List<WriterViewModel> GetEntityWritersList(int articleId)
        {
            var result = new List<WriterViewModel>();
            var writers = _writerInArticelRepository.GetWritersByArticleId(articleId);
            foreach (var writer in writers)
            {
                result.Add(new WriterViewModel
                {
                    Id = writer.Id,
                    Name = writer.Name,
                    BirthDate = writer.BirthDate
                });
            }
            return result;
        }
        public void CreateWriter(WriterViewModel writer, int entityId)
        {
            var entity = new Writer
            {
                Name = writer.Name,
                BirthDate = writer.BirthDate
            };
            var tempArticle = _articleRepository.Find(article => article.Id == entityId).First();
            var tempWriterInArticle = new WriterInArticle();
            tempWriterInArticle.Writer = entity;
            tempWriterInArticle.Article = tempArticle;
            tempArticle.WriterInArticles.Add(tempWriterInArticle);
            entity.WriterInArticles.Add(tempWriterInArticle);
            _writerRepository.Create(entity);
            _writerInArticelRepository.Create(tempWriterInArticle);
            _articleRepository.Update(tempArticle);
            _writerInArticelRepository.Save();
            writer.Id = _writerRepository.Last().Id;
        }
        public void UpdateWriter(WriterViewModel writer)
        {
            var newWriter = new Writer
            {
                Id = writer.Id,
                Name = writer.Name,
                BirthDate = writer.BirthDate,
            };
            _writerRepository.Update(newWriter);
            _writerRepository.Save();
        }
        public void DestroyWriter(WriterViewModel writer)
        {
            _writerRepository.Delete(writer.Id);
            _writerRepository.Save();
        }
        public List<ArticleViewModel> getArticlesList()
        {
            var result = new List<ArticleViewModel>();
            var articles= _articleRepository.GetAll();
            foreach (var article in articles)
            {
                result.Add(new ArticleViewModel
                {
                    Id = article.Id,
                    Name = article.Name,
                    PublishDate = article.PublishDate
                });
            }
            return result;
        }
        public void CreateArticle(ArticleViewModel viewModel)
        {
            var entity = new Article
            {
                Name = viewModel.Name,
                PublishDate = viewModel.PublishDate
            };
            _articleRepository.Create(entity);
            _articleRepository.Save();
            viewModel.Id = _articleRepository.Last().Id;
        }

        public void UpdateArticle(ArticleViewModel viewModel)
        {
            var entity = new Article
            {
                Id=viewModel.Id,
                Name = viewModel.Name,
                PublishDate = viewModel.PublishDate
            };
            _articleRepository.Update(entity);
            _articleRepository.Save();
        }

        public void DestroyArticle(ArticleViewModel viewModel)
        {
            _articleRepository.Delete(viewModel.Id);
            _articleRepository.Save();
        }
        public void Dispose()
        {
            _writerInArticelRepository.Dispose();
            _writerRepository.Dispose();
            _articleRepository.Dispose();
        }
    }
}
